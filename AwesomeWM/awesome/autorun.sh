#!/usr/bin/env bash

#function run {
#  if ! pgrep -f $1 ;
#  then
#    $@&
#  fi
#}

killall compton
killall feh
killall cbatticon
killall mpv
killall xbindkeys

xbindkeys --poll-rc
compton &
cbatticon &
feh --random --bg-scale ~/Wallpapers/
# xwinwrap -ov -ni -fs -- mpv -vo x11 -wid WID --keepaspect=no --loop /home/soda/Wallpapers/fart.gif
